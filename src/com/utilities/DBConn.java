package com.utilities;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

public class DBConn {
	private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/java_quizgame";
	private static final String DB_USER = "java_quizgame";
	private static final String DB_PASSWORD = "JXJ6JcajcsTG9DbY";

	public static Connection getDBConnection() {
		Connection dbConnection = null;

		try {
			Class.forName(DB_DRIVER);
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return dbConnection;
	}
}
