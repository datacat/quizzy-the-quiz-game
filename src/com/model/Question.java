package com.model;

public class Question {
	private String qID, query, answer, decoy1, decoy2, decoy3;
	public Question() {
		qID = "empty";
		query = "empty";
		answer = "empty";
		decoy1 = "empty";
		decoy2 = "empty";
		decoy3 = "empty";
	}
	
	public Question(String qID, String query, String answer, String decoy1, String decoy2, String decoy3) {
		super();
		this.qID = qID;
		this.query = query;
		this.answer = answer;
		this.decoy1 = decoy1;
		this.decoy2 = decoy2;
		this.decoy3 = decoy3;
	}

	public String getqID() {
		return qID;
	}
	
	public void setqID(String qID) {
		this.qID = qID;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getDecoy1() {
		return decoy1;
	}

	public void setDecoy1(String decoy1) {
		this.decoy1 = decoy1;
	}

	public String getDecoy2() {
		return decoy2;
	}

	public void setDecoy2(String decoy2) {
		this.decoy2 = decoy2;
	}

	public String getDecoy3() {
		return decoy3;
	}

	public void setDecoy3(String decoy3) {
		this.decoy3 = decoy3;
	}
}
