package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.model.Question;

public class QuestionDAO {

	public static List<Question> fetchQuestions(String topic) throws SQLException {
		List<Question> returnList = new ArrayList<Question>();

		Connection dbConnection = null;
		Statement statement = null;

		String sqlSelect = String.format("SELECT * FROM qtopic_%s ORDER BY RAND()", topic);

		try {
			dbConnection = com.utilities.DBConn.getDBConnection();
			statement = dbConnection.createStatement();

			// execute select SQL statement
			ResultSet rs = statement.executeQuery(sqlSelect);
			
			while (rs.next()) {
				String res_qID		= rs.getString("qID");
				String res_query	= rs.getString("query");
				String res_answer	= rs.getString("correct");
				String res_decoy1	= rs.getString("decoy1");
				String res_decoy2	= rs.getString("decoy2");
				String res_decoy3	= rs.getString("decoy3");

				Question new_question = new Question(res_qID, res_query, res_answer, res_decoy1, res_decoy2, res_decoy3);
				returnList.add(new_question);
			}

		} catch (SQLException e) {


		} finally {

			if (statement != null) {
				statement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}
		}

		return returnList;
	}
	
}