package com.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.model.Question;

/**
 * Servlet implementation class AskQuestion
 */
@WebServlet(name = "Ask", urlPatterns = { "/Ask" })
public class AskQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AskQuestionServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Get references to the context and session
		HttpSession session = request.getSession();
		ServletContext sc = this.getServletContext();
		RequestDispatcher rd;
		
		//Set default values
		Question questionData;
		String feedbackClass = "", feedbackMsg = "";
		String topic = (request.getParameter("topic") == null)? "food" : request.getParameter("topic");

		//Get the current game if it exists
		QuizGame quizController = (QuizGame)session.getAttribute("game");
		
		if((quizController != null) && (quizController.getTime() > 0)){ 
			//this is an existing game
			quizController.incrementCounter();
			quizController.setTime(request.getParameter("remainingTime"));
			
			if(request.getParameter("submittedQuestionID") != null){
				//there was a previous question to grade
				if(quizController.gradeQuestion(request.getParameter("submittedQuestionID"), request.getParameter("submittedAnswer"))){
					feedbackClass = "ansCorrect";
					feedbackMsg = "Correct!<br>Ready for the next one?";
				}
				else{
					feedbackClass = "ansIncorrect";
					feedbackMsg = "Incorrect... <br>But keep going!";
				}
			}
		} 
		else {
			//this is a new game
			try {
				quizController = new QuizGame(topic);
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}

		if(quizController != null){
			//update game data in the session
			session.setAttribute("game", quizController);
	
			//select the next question
			questionData = quizController.nextQuestion();
	
			if(questionData != null){ //we still have questions
				//send data to the JSP
				request.setAttribute("feedbackClass", feedbackClass);
				request.setAttribute("feedbackMsg", feedbackMsg);
				request.setAttribute("loadedQuestion", questionData);
				
				//forward program control to JSP
				rd = sc.getRequestDispatcher("/Question.jsp");
				rd.forward(request, response);
			}
			else{ //we're out of questions, Game over!
				quizController.decrementCounter();
				rd = sc.getRequestDispatcher("/GameOver.jsp");
				rd.forward(request, response);
			}
		}
		else{
			//forward program control to Error.JSP
			rd = sc.getRequestDispatcher("/Error.jsp");
			rd.forward(request, response);

		}
	}

}
