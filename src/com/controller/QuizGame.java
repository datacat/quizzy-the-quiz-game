package com.controller;

import java.util.ArrayList;
import java.util.List;

import com.dao.QuestionDAO;
import com.model.Question;

public class QuizGame {	
	private int counter, time, score;
	private List<Question> questionList;
	private List<Question> spentQuestions;

	public QuizGame(String topic) throws Exception {
		counter = 1;
		time = 30;
		score = 0;

		//create the question lists
		spentQuestions = new ArrayList<Question>();
		questionList = QuestionDAO.fetchQuestions(topic);
	}

	//Getters and Setters
	public int getCounter() {
		return counter;
	}
	
	public void incrementCounter() {
		counter += 1;
	}
	
	public void decrementCounter(){
		counter -= 1;
	}
	
	public int getTime() {
		return time;
	}
	
	public void setTime(String time) {
		try{
			this.time = Integer.parseInt(time);
		}
		catch(Exception e){
			this.time = 0;
		}
	}
	
	public int getScore() {
		return score;
	}
	
	//Public Methods
	public Question nextQuestion(){		
		if(questionList.size() > 0){
			Question q = questionList.get(0);
			questionList.remove(0);
			spentQuestions.add(q);
			return q;
		}

		return null;
	}
	
	public boolean gradeQuestion(String id, String userAnswer){	
		for(Question q : spentQuestions){
			if(q.getqID().equals(id)){
				if(q.getAnswer().equals(userAnswer)){
					time+=10;
					score+=1;
					return true;
				}
				else{
					time-=10;
					return false;
				}
			}
		}
		
		return false;
	}	
}
