-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2014 at 11:58 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quizgame`
--
CREATE DATABASE IF NOT EXISTS `quizgame` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `quizgame`;

-- --------------------------------------------------------

--
-- Table structure for table `qtopic_food`
--

CREATE TABLE IF NOT EXISTS `qtopic_food` (
  `qID` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(255) NOT NULL,
  `correct` varchar(255) NOT NULL,
  `decoy1` varchar(255) NOT NULL,
  `decoy2` varchar(255) DEFAULT NULL,
  `decoy3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`qID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Truncate table before insert `qtopic_food`
--

TRUNCATE TABLE `qtopic_food`;
--
-- Dumping data for table `qtopic_food`
--

INSERT IGNORE INTO `qtopic_food` (`qID`, `query`, `correct`, `decoy1`, `decoy2`, `decoy3`) VALUES
(1, 'Europeans first learned of chocolate from whom?', 'Aztecs', 'Africans', 'East Indians', 'New Zealanders'),
(2, 'In the United States, about how much beer does the average person drink each year?', '24 gallons', '24 quarts', '24 pints', '24 ounces'),
(3, 'Of all commercial cooking oils, which of these is highest in polyunsaturates and lowest in saturated fat?', 'Safflower oil', 'Olive oil', 'Corn oil', 'Coconut oil'),
(4, 'Which of the following is not a favorable adjective when discussing wine?', 'Fat', 'Flinty', 'Leggy', 'Vigorous'),
(5, 'Which of the following vegetables is not one of the ingredients of V-8 juice?', 'Cabbage', 'Beet', 'Carrot', 'Spinach'),
(6, 'What is the main ingredient in vichyssoise?', 'Potatoes', 'Tomatoes', 'Clams', 'Lima beans'),
(7, 'What country produces the most potatoes?', 'China', 'United States', 'Ireland', 'Russia'),
(8, 'What soft-drink company introduced the brand Slice?', 'Coca Cola', 'Dr. Pepper', 'Seven Up', 'Pepsico'),
(9, 'According to a 1980s Beverage Media poll of four hundred bartenders, what is the average male customers favorite drink?', 'Beer', 'Margarita', 'Peach schnapps and orange juice', 'White wine'),
(10, 'According to a 1980s Beverage Media poll of four hundred bartenders, what was the female customers favorite drink?', 'White wine', 'Beer', 'Margarita', 'Peach schnapps and orange juice'),
(11, 'Simplesse is NutraSweet''s fat substitute.  What is it made of?', 'A blend of proteins from egg white and milk', 'Fat molecules altered to be too large to digest', 'Molecules that are the mirror-image of normal fat molecules', 'Unstable fat molecules that break down before digestion'),
(12, 'Which grade of olive oil is considered the best?', 'Extra virgin', 'Pure virgin', 'Super virgin', 'Ultra virgin'),
(13, 'What vegetable has varieties known as Bell Tower, Orobelle, and Jupiter?', 'Pepper', 'Onion', 'Squash', 'Tomato'),
(14, 'In the drink called a zombie, what is the main alcoholic ingredient?', 'Rum', 'Beer', 'Brandy', 'Whiskey'),
(15, 'Of the following dishes, which are not typically made with some kind of seafood?', 'Osso buco', 'Bouillabaisse', 'Fritto misto', 'Tempura'),
(16, 'Which of the following ingredients are not used in a Bloody Mary according to Playboy Bar Guide?', 'Sugar', 'Ketchup', 'Worcestershire sauce', 'Tabasco sauce'),
(17, 'Which of the following compounds have not been approved for use in the U.S. as an artificial sweetener?', 'Acetaminophen', 'Acesulfame K', 'Aspartame', 'Saccharine'),
(18, 'The original Bellini was a mixture of sparkling Italian white wine and what type of fruit juice?', 'Peach', 'Pomegranate', 'Orange', 'Apple'),
(19, 'The sandwich known as the ''Reuben'' does not have which of the following ingredients?', 'Boiled ham', 'Corned Beef', 'Sauerkraut', 'Swiss Cheese'),
(20, 'Marzipan is made with what kind of nut?', 'Almond', 'Cashew', 'Walnut', 'Pecan'),
(21, 'Which country is the world''s largest exporter of quality wines?', 'France', 'Australia', 'Portugal', 'South Africa'),
(22, 'Which place in France is known for its great red wines?', 'Bordeaux', 'Champagne', 'Loire', 'Paris'),
(23, 'Identify the place in Germany which makes the most expensive wines.', 'Mosel-Saar-Ruwer', 'Carneros', 'Loire', 'Sonoma'),
(24, 'With time, white wines usually...', 'deteriorate', 'improve', 'increase in volume', 'increase in weight'),
(25, 'As they age, red wines usually...', 'become lighter in color', 'remain unchanged in color', 'become outdated', 'become darker in color'),
(26, 'Descending streaks of wine seen on the interior of a glass after it has been swirled are called?', 'legs', 'stems', 'ribbons', 'strings'),
(27, 'Which country is the world''s third largest exporter of quality wines?', 'Spain', 'France', 'Australia', 'South Africa'),
(28, 'Tetrodotoxin poisoning is associated with the consumption of which one of the following food items?', 'Fugu', 'Oysters', 'Wild mushrooms', 'Moldy grains'),
(29, 'Table sugar is which type of sugar?', 'Sucrose', 'Glucose', 'Galactose', 'Fructose'),
(30, 'Regular coffee, tea, and cola all contain which stimulant?', 'Caffeine', 'Sucrose', 'Theobromine', 'Theophylline'),
(31, 'Sodium bicarbonate is commonly used in cooking as?', 'baking soda', 'baking powder', 'cream of tartar', 'alum'),
(32, 'A tomato gets its red color from?', 'lycopene', 'fructose', 'beta carotene', 'limonene'),
(33, 'Leafy vegatable get most of their green color from?', 'chlorophyll', 'cerotene', 'mitrochondira', 'xanthophyll'),
(34, 'The bubbles in champagne and soda are?', 'carbon dioxide', 'hydrogen', 'nitrogen', 'oxygen'),
(35, 'Hot peppers get their heat from?', 'capsaicin', 'acetic acid', 'lycopene', 'sulfuric acid'),
(36, 'Table salt is?', 'sodium chloride', 'iodine', ' potassium chloride', ' sodium bicarbonate'),
(37, 'When you chop onions, your eyes can burn because a chemical reaction produces...', 'sulfuric Acid', 'acetic Acid', 'nitric Acid', 'hydrochloric Acid'),
(38, 'The type of alcohol in beverages such as wine, beer, and vodka is...', 'ethyl alcohol (Ethanol)', 'isopropyl alcohol (Isopropanol)', 'methyl alcohol (aethanol)', 'propyl alcohol (Propanol)'),
(39, 'The rice dish "paella" comes from what country?', 'Spain', 'Italy', 'Greece', 'Japan'),
(40, 'Deer meat is known by what name?', 'Venison', 'Venusian', 'Version', 'Venetian'),
(41, 'What food is used as the base of guacamole?', 'Avocado', 'Ground black pepper', 'Apple', 'Artichoke');

-- --------------------------------------------------------

--
-- Table structure for table `qtopic_javaservlets`
--

CREATE TABLE IF NOT EXISTS `qtopic_javaservlets` (
  `qID` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(255) NOT NULL,
  `correct` varchar(255) NOT NULL,
  `decoy1` varchar(255) NOT NULL,
  `decoy2` varchar(255) DEFAULT NULL,
  `decoy3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`qID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Truncate table before insert `qtopic_javaservlets`
--

TRUNCATE TABLE `qtopic_javaservlets`;
--
-- Dumping data for table `qtopic_javaservlets`
--

INSERT IGNORE INTO `qtopic_javaservlets` (`qID`, `query`, `correct`, `decoy1`, `decoy2`, `decoy3`) VALUES
(1, 'The initials JSP stand for...', 'Java Server Pages', 'Java Simple Processing', 'Java Server Programming', 'Java Special Pages'),
(2, 'The initials JSF stand for...', 'Java Server Faces', 'Java Serialization Fractions', 'Java Sectioning Facility', 'Java Server Facelets'),
(3, 'Which company produces Tomcat?', 'Apache', 'Navajo', 'Microsoft', 'Apple'),
(4, 'Tomcat is primarily a(n) _____ server.', 'application', 'database', 'web', 'programming'),
(5, 'Servlet code mainly goes into the _____ methods', 'doGet and doPost', 'doThis and DoThat', 'doMethod and doFunction', 'doRight and doWrong'),
(6, 'Which object in a servlet contains outgoing information?', 'HttpServletResponse', 'HttpServletRequest', 'HttpServletReadline', 'HttpServiceResponder'),
(7, 'Which of the following methods will send data appended to the URL?', 'GET', 'POST', 'REQUEST', 'SEND'),
(8, 'Which of the following methods will send data with the request headers', 'POST', 'GET', 'TRACE', 'HEAD'),
(9, 'When building an Eclipse Dynamic Web Project, HTML files go in which project folder?', 'WebContent', 'FileServe', 'Meta-INF', 'src'),
(10, 'Which of the following is NOT an advantage of using GET?', 'Can send special characters and uploaded files', 'Can bookmark the resulting page', 'Browsers can cache results', 'Easy to test interactively'),
(11, 'When writing XHTML output, tag names and attribute names... ', 'must be lowercase', 'must be uppercase', 'must be pascal case', 'must be camel case'),
(12, 'Form data can be read in a servlet using the _____ method(s)', 'getParameter', 'getPOSTData and getGETData', 'readSource', 'getParameterByName'),
(13, 'Check for missing form data by...', 'looking for null values and empty strings', 'using the integrated form validator object', 'including the txtValidator jar', 're-entering all form data'),
(14, 'Which HTTP header indicates what MIME types a browser can handle?', 'Accept', 'Accept Encoding', 'Connection', 'Referrer'),
(15, 'Which HTTP header indicates what encodings the browser can handle?', 'Accept Encoding', 'Accept', 'User Agent', 'Authorization'),
(16, 'A _____ lets the server reuse the same socket for repeat connections with the same client', 'persistent connection', 'constant link', 'linked authorization', 'persistent authorization'),
(17, 'Which header can be used to determine if a user is on a mobile phone?', 'User-Agent', 'Connection', 'Referrer', 'Session'),
(18, 'Which of the following is NOT a quality of the HTTP referrer header?', 'It contains the reference code of the document', 'It contains the URL of the referring website', 'It can be easily spoofed', 'It may be screened out by some browsers'),
(19, 'Which HTTP response header indicates the MIME type of the document being returned?', 'Content-Type', 'Accept-Encoding', 'MIME-Type', 'Return-Document'),
(20, 'Attach a cookie to the server response using the _____ method', 'response.addCookie', 'response.setCookie', 'response.includeCookie', 'response.newCookie'),
(21, 'To get an array of cookie objects use the _____ method', 'request.getCookies', 'response.getCookies', 'request.listCookies', 'response.listCookies'),
(22, 'To update the value of an existing cookie, ...', 'send a cookie of the same name but with a different value', 'use the setValue method', 'restart the browser', 'access the application from a different host'),
(23, 'Cookies should NOT be used to... ', 'store credit card information', 'detect first time visitors', 'allow users to customize a site', 'focus advertising'),
(24, 'Call _____ to get the HttpSession object', 'request.getSession', 'response.getSession', 'request.includeSession', 'response.getAttribute'),
(25, 'Call _____ to discard an entire session', 'invalidate', 'delete', 'escape', 'discard'),
(26, 'Which method returns all of the attribute names in the current session?', 'getAttributeNames', 'attribute.getNames', 'session.getAttributes', 'getAllNames'),
(27, 'Servlets make it easy to do all of the following EXCEPT', 'Generate an HTML response', 'Read form data', 'Read HTTP request headers', 'Use cookies and session tracking'),
(28, 'A _____ allows java code to be run directly from a jsp without using xml syntax', 'scriptlet', 'midlet', 'servlet', 'applet'),
(29, 'Which of the following variables is predefined in a JSP file', 'out', 'applet', 'responder', 'in'),
(30, 'JSP is...', 'translated into a servlet', 'non transferable between servers', 'developed by Apache', 'impossible to run on Macintosh computers'),
(31, 'It is not possible to override init and destroy from a JSP; use _____ and _____ instead', 'jspInit, jspDestroy', 'servletInit, servletDestroy', 'Initiate, Discard', 'Instantiate, Deallocate'),
(32, 'The page directive can control all of the following EXCEPT...', 'whether search engines can index the page', 'which classes are imported', 'what MIME type is generated', 'If the servlet participates in sessions'),
(33, 'The _____ attribute of the page directive gives the size of the buffer used by the out variable', 'buffer', 'out', 'header', 'writer'),
(34, 'The isErrorPage attribute of the page directive...', 'indicates whether the current page is an error page', 'denotes the location of the error page', 'sends an error message to the server', 'throws an exception by default'),
(35, 'The ''extends'' attribute of the page directive...', 'should be used with extreme caution', 'is enabled by default', 'is ignored by the front end processor', 'generates a servlet that extends the current page'),
(36, 'To reuse JSP, HTML or plain text content use...', 'jsp:include', 'jsp:import', 'jsp-include', 'jsp-import');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
