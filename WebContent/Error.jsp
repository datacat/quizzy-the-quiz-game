<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/baseTemplate.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>Quizzy!</title>
<!-- InstanceEndEditable -->
<meta name="generator" content="Bootply" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<link href="css/styles.css" rel="stylesheet">
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="masthead">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1 class="lead">Quizzy!</h1>
            </div>
            <div class="col-md-5"> 
                <div class="well well-lg">
                    <div class="row">
                        <div class="col-sm-12">
						<!-- InstanceBeginEditable name="masthead-well" -->
						<h2>The Rules</h2>
                        <p>This game moves quick so you'd better answer fast!<br />
                            You start with 30 seconds; answer right and you get 10 more, answer wrong and you get 10 less.<br />
    <strong>Alright, GO GO GO!</strong></p>
						<!-- InstanceEndEditable -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top-spacer">&nbsp;</div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
            	<div class="panel-body">
				<!-- InstanceBeginEditable name="body-content" -->
                    <h1>Oh Snap!</h1>
                    <p>A fatal error occured and I couldn't start your game. I'm really sorry about that.</p>
                    
                    <p><a href="index.html">Try again?</a></p>
<!-- InstanceEndEditable --> 
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-5">
                <h4 class="text-center">Built by Robin Mendzies</h4>
            </div>
            <div class="col-xs-7">
                <h4 class="text-center"><a href="https://bitbucket.org/datacat/quizzy-the-quiz-game" target="_self">Visit this project on BitBucket</a></h4>
            </div>
            <div class="col-xs-4">
            </div>
        </div>
    </div>
</footer>
<script src="js/jquery-1.11.2.min.js"></script> 
<script src="js/bootstrap.min.js"></script>
<!-- InstanceBeginEditable name="scripts" -->
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>