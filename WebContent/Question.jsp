<%@ page import="com.model.Question"%>
<%@ page import="com.controller.QuizGame" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
	QuizGame quizController = (QuizGame)session.getAttribute("game");
		int remainingTime = quizController.getTime();
		int counter = quizController.getCounter();
	
	String feedbackBanner = String.format("<div class=%s>%s</div>", (String) request.getAttribute("feedbackClass"), (String) request.getAttribute("feedbackMsg"));
	
	Question loadedQuestion = (Question) request.getAttribute("loadedQuestion");
%>
<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/baseTemplate.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>Quizzy!</title>
<!-- InstanceEndEditable -->
<meta name="generator" content="Bootply" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<link href="css/styles.css" rel="stylesheet">
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<div id="masthead">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1 class="lead">Quizzy!</h1>
            </div>
            <div class="col-md-5"> 
                <div class="well well-lg">
                    <div class="row">
                        <div class="col-sm-12">
						<!-- InstanceBeginEditable name="masthead-well" -->
                            <h3><%=feedbackBanner %></h3>
                            <h3><i class="glyphicon glyphicon-time"></i> Time Remaining <span id="countDown"></span></h3>
                            <!-- InstanceEndEditable -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top-spacer">&nbsp;</div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
            	<div class="panel-body">
				<!-- InstanceBeginEditable name="body-content" -->
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Question #<%=counter %>!</h2>
                            <p><%=loadedQuestion.getQuery() %></p>
                        </div>
                        <div class="col-sm-6">
                            <form method="post" id="AnswerForm" action="Ask" role="form">
                                <input type="hidden" name="submittedQuestionID" value="<%=loadedQuestion.getqID() %>">
                                <input type="hidden" name="remainingTime" id="remainingTime">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="submittedAnswer" value="<%=loadedQuestion.getAnswer() %>">
                                        <%=loadedQuestion.getAnswer() %>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="submittedAnswer" value="<%=loadedQuestion.getDecoy1() %>">
                                        <%=loadedQuestion.getDecoy1() %>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="submittedAnswer" value="<%=loadedQuestion.getDecoy2() %>">
                                        <%=loadedQuestion.getDecoy2() %>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="submittedAnswer" value="<%=loadedQuestion.getDecoy3() %>">
                                        <%=loadedQuestion.getDecoy3() %>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <button type="submit" id="submitButton" class="btn btn-primary" disabled>That's my Final Answer</button>
                                    <button type="button" id="quitButton" class="btn btn-default">Quit this game</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- InstanceEndEditable --> 
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-5">
                <h4 class="text-center">Built by Robin Mendzies</h4>
            </div>
            <div class="col-xs-7">
                <h4 class="text-center"><a href="https://bitbucket.org/datacat/quizzy-the-quiz-game" target="_self">Visit this project on BitBucket</a></h4>
            </div>
            <div class="col-xs-4">
            </div>
        </div>
    </div>
</footer>
<script src="js/jquery-1.11.2.min.js"></script> 
<script src="js/bootstrap.min.js"></script>
<!-- InstanceBeginEditable name="scripts" --> 
<script src="js/shuffle.js"></script> 
<script type="text/javascript">
	//get the remaining time from the server
	var rt = <%=remainingTime %>;

	//redirect the user to the game over screen	
	function endGame(){
		window.location.replace("GameOver.jsp");
	}
	setTimeout(endGame, (rt * 1000));
	
	//update the clock
	function updateClock(){
		var secs = (rt > 9)? rt % 60 : '0' + (rt % 60);
		var mins = (rt - secs) / 60;
		$('#countDown').html(mins + ':' + secs);
	}
	
	function tick(){
		rt--;
		updateClock();
	}
	setInterval(tick, 1000);	
	
	//ONLOAD shuffle the answers
	$(document).ready(function(){
		updateClock();
		$('#AnswerForm .radio').shuffle();
	});
	
	//ONSUBMIT pass remaining time from javascript back to server side
 	$('#AnswerForm').on('submit', function(){
		$('#remainingTime').val(rt);
 	});
	
	//ONCLICK redirect user to game over screen when they quit
	$('#quitButton').click(function(){
		endGame();
	});
	
	//ONCLICK enable submit button
	$('#AnswerForm label').click(function(){
		$('#submitButton').removeAttr('disabled');
		
	});
</script> 
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>